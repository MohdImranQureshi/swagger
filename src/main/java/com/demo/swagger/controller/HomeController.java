package com.demo.swagger.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Content from database")
public class HomeController {

	private static Logger logger = LoggerFactory.getLogger(HomeController.class);

	@ApiOperation(value = "get data from database", response = String.class, responseContainer = "String")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "success", response = String.class, responseContainer = "String") })
	@GetMapping(path = "/getData")
	public ResponseEntity<String> getDataFromDB() {
		logger.info("Entering into HomeController.getData()");
		String response = "Getting data from Database";
		logger.info("Leaving from HomeController.getData()");
		return new ResponseEntity<String>(response, HttpStatus.OK);

	}

	@ApiOperation(value = "save data into DB", response = String.class, responseContainer = "String")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "success", response = String.class, responseContainer = "String") })
	@PostMapping(path = "/saveData")
	public ResponseEntity<String> saveDataIntoDB() {
		logger.info("Entering into HomeController.saveDataIntoDB()");
		String response = "Details Successfully saved.";
		logger.info("Leaving from HomeController.saveDataIntoDB()");
		return new ResponseEntity<String>(response, HttpStatus.CREATED);
	}

}
