package com.demo.swagger.exception;

import org.springframework.http.HttpStatus;

public class SwaggerServiceException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	HttpStatus status;
	
	public SwaggerServiceException(final String message){
		super(message);
	}
	
	public SwaggerServiceException(final String message, HttpStatus status){
		super(message);
		this.status = status;
	}
	
	public HttpStatus getStatus() {
        return status;
    }


}
