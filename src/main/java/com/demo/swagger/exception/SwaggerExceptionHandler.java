package com.demo.swagger.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ControllerAdvice
public class SwaggerExceptionHandler {

	private static Logger logger = LoggerFactory.getLogger(SwaggerServiceException.class);
	
	@ExceptionHandler(ServletRequestBindingException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<Object> handle(ServletRequestBindingException ex){
		logger.error("Bad Request");
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
		
	}
	
}
